package diceware


import (
	"fmt"
)


func Pwgen() {
	w := Dice.Roll

	fmt.Print("Words taken from EFF's long wordlist (7776 words)..\n")

	fmt.Print("\nKDF-passphrase, ~128-bit:\n\t")
	for i := 0; i < 10; i++ {
		//  print 6 words a row
		if (i+1) % 5 == 0 {
			fmt.Print(w() + "\n\t")
		} else {
			fmt.Print(w() + " ")
		}
	}

	fmt.Print("\nKDF-nonce ~128-bit:\n\t")
	for i := 0; i < 10; i++ {
		if (i+1) % 5 == 0 {
			fmt.Print(w() + "\n\t")
		} else {
			fmt.Print(w() + " ")
		}
	}

	fmt.Println("\nHKDF-salt (example: user@service/salt):\n\t" + w())
}
