package diceware


import (
	"crypto/rand"
	"math/big"
)


var Dice = NewDiceware()


type Diceware struct {
	list []string
	size *big.Int
}


func NewDiceware() (this Diceware) {
	this.list = getWords()
	this.size = big.NewInt(int64(len(this.list)))
	return
}


func (this *Diceware) Roll() string {
	n, err := rand.Int(rand.Reader, this.size)
	if err != nil {
		panic(err)
	}
	return this.list[n.Int64()]
}
