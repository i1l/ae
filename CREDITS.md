# Credits

***This program is based on other's work, here goes the credit:***

- Amar M. Singh et all, for providing some real life distractions from this  
@ https://codeberg.org/nly

- The Go Authors, for Go and its everything  
@ go.dev

- Joseph Bonneau, for EFF's Large Wordlist  
@ www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases

- Electronic Frontier Foundation, for their blogs  
@ www.eff.org
