package main


import (
	_ "embed"
	"encoding/hex"
	"os"

	"ae/diceware"
	"ae/kdf"
)


func usage() {
	os.Stderr.Write([]byte(
		`
See Credits:
  cmd credits

See License:
  cmd license

Generate a Diceware-like passphrase:
  cmd pwgen

Prompt for credentials, printout the derived key:
  cmd derive` + "\n\n"))

	os.Exit(1)
}


//go:embed CREDITS.md
var credits []byte

//go:embed LICENSE.md
var license []byte


func main() {

	argc := len(os.Args)
	if argc < 2 {    //  [0] = progname
		usage()
	}

	switch job := os.Args[1]; job {
	case "credits","c":
		os.Stdout.Write(credits)
	case "license","l":
		os.Stdout.Write(license)
	case "pwgen","p":
		diceware.Pwgen()
	case "derive","d":
		os.Stdout.Write([]byte(hex.EncodeToString(kdf.DeriveKey(32))))
	default:
		usage()
	}
}
