package diceware


import (
	"ae/diceware/credits/eff"
)


func getWords () []string {
	return eff.GetLargeWordlist()
}
