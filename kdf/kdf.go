package kdf


import (
	"hash"
	"io"

	"ae/ui"

	"golang.org/x/crypto/argon2"
	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/hkdf"
)


func newHash256() hash.Hash {
	h, err := blake2b.New256(nil)
	if err != nil {
		panic(err)
	}
	return h
}


func DeriveKey(keyLen uint32) []byte {
	pass := ui.Prompt("KDF-passphrase: ")
	nonce := ui.Prompt("KDF-nonce: ")
	salt := ui.Prompt("HDKF-salt: ")

	mainKey := argon2.IDKey(
		pass,
		nonce,
		16,          //  iterations
		64 * 1024,  //  RAM usage, in KiB
		4,           //  parallelism, AFFECTS the result
		keyLen)     //  result's length in bytes

	deriver := hkdf.New(newHash256, mainKey, salt, nil)

	key := make([]byte, keyLen)
	_, err := io.ReadFull(deriver, key[:])
	if err != nil {
		panic(err)
	}

	return key[:]
}
