# ae

A Diceware-like "pwgen", also PBKDF + HKDF wrapper.

# Build

Run `go build` at the root dir of this git tree.

# Usage

Run `./ae` to see the usage info.

Basically user first runs `./ae p` to get the memorable credentials.
Then `./ae d`, which command prompts for passphrase and nonce, so Argon2
can generate a master key.  Also it prompts for salt, which used by HKDF
to derive a subkey.

The master key is used only to generate the subkeys, while subkeys can be
used as actual 'passwords'.  As i understood, HKDF can do job even without
salt, so the salt is actually used to make the different subkeys from same
master-key.

# Disclaimer

IDK what i am doing, but it was fun to do some code.
