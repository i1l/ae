package ui


import (
	"bufio"
	"os"
)


var stdin *bufio.Reader = bufio.NewReader(os.Stdin)


func Prompt (p string) (input []byte) {
	var err error

	//  echo the prompt
	_, err = os.Stderr.Write([]byte(p))
	if err != nil {
		panic(err)
	}

	//  read the input
	input, err = stdin.ReadBytes('\n')
	if err != nil {
		panic(err)
	}

	return
}
